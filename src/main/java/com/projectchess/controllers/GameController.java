package com.projectchess.controllers;

import com.projectchess.controllers.dto.response.GetGameResponse;
import com.projectchess.model.Game;
import com.projectchess.model.User;
import com.projectchess.repository.GameRepository;
import com.projectchess.services.GameService;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Set;
import java.util.UUID;

import static java.util.stream.Collectors.toSet;

@RepositoryRestController
@RequestMapping("/game")
public class GameController {
    private final GameService gameService;
    private final GameRepository gameRepository;

    public GameController(GameService gameService, GameRepository gameRepository) {
        this.gameService = gameService;
        this.gameRepository = gameRepository;
    }

    @GetMapping("/active")
    @ResponseBody
    public Set<Game> getActiveGames() {
        return gameRepository.findByInProgressIsTrue();
    }

    @GetMapping("/abandon/{id}")
    @ResponseBody
    public void abandonGame(@PathVariable UUID id) {
        Game game = gameService.findById(id);
        gameService.abandon(game);
    }

    @PostMapping
    @ResponseBody
    public Game createGame() {
        return gameService.save();
    }

    @GetMapping("/{id}")
    @ResponseBody
    public GetGameResponse getGame(@PathVariable UUID id) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Game game = gameRepository.findByIdAndWhitePlayerAndInProgressIsTrue(id, user);
        return new GetGameResponse(
                game.getId().toString(),
                game.getName(),
                game.getPieces().stream().map((piece) -> new GetGameResponse.Piece(
                                piece.getId().toString(),
                                piece.getColor(),
                                piece.getType(),
                                piece.getPosX(),
                                piece.getPosY()
                                )
                        ).collect(toSet())
        );
    }

}
