package com.projectchess.controllers;

import com.projectchess.config.security.JwtUtils;
import com.projectchess.controllers.dto.request.LoginRequest;
import com.projectchess.controllers.dto.response.UserDetails;
import com.projectchess.model.User;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import static com.projectchess.config.security.JwtUtils.AUTH_HEADER_NAME;

@RestController
@RequestMapping("/api")
public class AuthController {
    private final AuthenticationManager authenticationManager;
    private final JwtUtils jwtUtils;

    public AuthController(AuthenticationManager authenticationManager, JwtUtils jwtUtils) {
        this.authenticationManager = authenticationManager;
        this.jwtUtils = jwtUtils;
    }

    @PostMapping("/login")
    @ResponseBody
    public UserDetails login(@Valid @RequestBody LoginRequest loginRequest, HttpServletResponse response) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        User user = (User) authentication.getPrincipal();

        response.addCookie(new Cookie(AUTH_HEADER_NAME, jwtUtils.generateJwtToken(authentication)));

        return new UserDetails(
                user.getId().toString(),
                user.getUsername()
        );
    }

    @GetMapping("/user-details")
    @ResponseBody
    public UserDetails getUserDetails() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return new UserDetails(
                user.getId().toString(),
                user.getUsername()
        );
    }
}
