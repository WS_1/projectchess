package com.projectchess.controllers.dto.response;

public class UserDetails {
    private final String id;
    private final String username;

    public UserDetails(String id, String username) {
        this.id = id;
        this.username = username;
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }
}
