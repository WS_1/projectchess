package com.projectchess.controllers.dto.response;

import com.projectchess.model.Piece.PieceColor;
import com.projectchess.model.Piece.PieceType;

import java.util.Set;

public class GetGameResponse {
        public static class Piece {
            private final String id;
            private final PieceColor color;
            private final PieceType type;
            private final Integer posX;
            private final Integer posY;

            public Piece(String id, PieceColor color, PieceType type, Integer posX, Integer posY) {
                this.id = id;
                this.color = color;
                this.type = type;
                this.posX = posX;
                this.posY = posY;
            }

            public String getId() {
                    return id;
                }

            public PieceColor getColor() {
                    return color;
                }

            public PieceType getType() {
                    return type;
                }

            public Integer getPosX() { return posX; }

            public Integer getPosY() { return posY; }
            }

    private final String id;
    private final String name;
    private final Set<Piece> pieces;

    public GetGameResponse(String id, String name, Set<Piece> pieces) {
        this.id = id;
        this.name = name;
        this.pieces = pieces;
    }

    public Set<Piece> getPieces() {
        return pieces;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
