package com.projectchess.model;

import com.projectchess.model.constraints.PiecesNumberConstraint;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@Entity
@Table
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Type(type = "uuid-char")
    private UUID id;

    @Column
    @NotNull
    private String name;

    @Column
    @NotNull
    private boolean inProgress = true;

    @ManyToOne(optional = false)
    @JoinColumn
    private User whitePlayer;

//    @ManyToOne(optional = false)
//    @JoinColumn
//    private User blackPlayer;

    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    //@PiecesNumberConstraint
    private Set<Piece> pieces;

    public Set<Piece> getPieces() {
        return pieces;
    }

    public void setPieces(Set<Piece> pieces) {
        this.pieces = pieces;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isInProgress() {
        return inProgress;
    }

    public void setInProgress(boolean inProgress) {
        this.inProgress = inProgress;
    }

    public User getWhitePlayer() { return whitePlayer; }

    public void setWhitePlayer(User whitePlayer) {this.whitePlayer = whitePlayer; }

//    public User getBlackPlayer() { return blackPlayer; }

//    public void setBlackPlayer(User blackPlayer) {this.blackPlayer = blackPlayer; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return getId().equals(game.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
