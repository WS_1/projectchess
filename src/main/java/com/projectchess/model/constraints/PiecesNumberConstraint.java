package com.projectchess.model.constraints;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = PawnNumberValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface PiecesNumberConstraint {
    String message() default "Invalid number of pieces.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
