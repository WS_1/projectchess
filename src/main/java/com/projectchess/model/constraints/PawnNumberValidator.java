package com.projectchess.model.constraints;

import com.projectchess.model.Piece;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Set;

public class PawnNumberValidator implements ConstraintValidator<PiecesNumberConstraint, Set<Piece>> {
    @Override
    public boolean isValid(Set<Piece> pawns,
                           ConstraintValidatorContext cxt) {
        return pawns.size() == 16;
    }
}