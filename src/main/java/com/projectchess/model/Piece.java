package com.projectchess.model;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Table
public class Piece {
    public enum PieceColor {
        WHITE,
        BLACK
    }

    public enum PieceType {
        PAWN,
        KING,
        QUEEN,
        BISHOP,
        KNIGHT,
        ROOK
    }

    public Piece() {
    }

    public Piece(@NotNull PieceType type, @NotNull PieceColor color, @NotNull @Range(min = 1, max = 8) Integer posX, @NotNull @Range(min = 1, max = 8) Integer posY) {
        this.type = type;
        this.color = color;
        this.posX = posX;
        this.posY = posY;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Type(type = "uuid-char")
    private UUID id;

    @Column
    @NotNull
    private PieceType type;

    @Column
    @NotNull
    private PieceColor color;

    @Column
    @NotNull
    @Range(min = 1, max = 8)
    private Integer posX;

    @Column
    @NotNull
    @Range(min = 1, max = 8)
    private Integer posY;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public PieceType getType() {
        return type;
    }

    public void setType(PieceType type) {
        this.type = type;
    }

    public PieceColor getColor() {
        return color;
    }

    public void setColor(PieceColor color) {
        this.color = color;
    }

    public Integer getPosX() {
        return posX;
    }

    public void setPosX(Integer posX) {
        this.posX = posX;
    }

    public Integer getPosY() {
        return posY;
    }

    public void setPosY(Integer posY) {
        this.posY = posY;
    }
}
