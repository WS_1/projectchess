package com.projectchess.services;

import com.projectchess.model.Game;
import com.projectchess.model.Piece;
import com.projectchess.model.User;
import com.projectchess.repository.GameRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.projectchess.model.Piece.PieceColor.BLACK;
import static com.projectchess.model.Piece.PieceColor.WHITE;
import static com.projectchess.model.Piece.PieceType.*;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Service
@PropertySource("classpath:gameNames.properties")
public class GameService {
    private final GameRepository gameRepository;
    private final Random random = new Random();
    @Value("${attributes}")
    private final List<String> attributes = new ArrayList<>();
    @Value("${animals}")
    private final List<String> animals = new ArrayList<>();

    public GameService(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public Game findById(UUID id) {
        return gameRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Game not found!"));
    }

    public Game save() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return gameRepository.save(loadGame(user));
    }

    public void abandon(Game game) {
        game.setInProgress(false);
        gameRepository.save(game);
    }

    private Game loadGame(User whitePlayer) {
        Game game = new Game();
        game.setName(generateName());
        game.setWhitePlayer(whitePlayer);

        Set<Piece> pieces = new HashSet<>();
        pieces.addAll(loadPawns());
        pieces.addAll(loadKings());
        pieces.addAll(loadQueens());
        pieces.addAll(loadBishops());
        pieces.addAll(loadKnights());
        pieces.addAll(loadRooks());

        game.setPieces(pieces);

        return game;
    }

    private Set<Piece> loadPawns() {
        return IntStream.rangeClosed(0, 15).mapToObj(num -> {
                    Piece.PieceColor color = WHITE;
                    int posX = num + 1;
                    int posY = 7;

                    if (num >= 8) {
                        color = BLACK;
                        posY = 2;
                        posX = num - 7;
                    }

                    return new Piece(PAWN, color, posX, posY);
                }
        ).collect(Collectors.toSet());
    }

    private Set<Piece> loadKings() {
        return Stream.of(
                new Piece(KING, WHITE, 4, 8),
                new Piece(KING, BLACK, 4, 1)
        )
                .collect(Collectors.toSet());
    }

    private Set<Piece> loadQueens() {
        return Stream.of(
                new Piece(QUEEN, WHITE, 5, 8),
                new Piece(QUEEN, BLACK, 5, 1))
                .collect(Collectors.toSet());
    }

    private Set<Piece> loadBishops() {
        return Stream.of(
                new Piece(BISHOP, WHITE, 3, 8),
                new Piece(BISHOP, BLACK, 3, 1),
                new Piece(BISHOP, WHITE, 6, 8),
                new Piece(BISHOP, BLACK, 6, 1)
        )
                .collect(Collectors.toSet());
    }

    private Set<Piece> loadKnights() {
        return Stream.of(
                new Piece(KNIGHT, WHITE, 2, 8),
                new Piece(KNIGHT, BLACK, 2, 1),
                new Piece(KNIGHT, WHITE, 7, 8),
                new Piece(KNIGHT, BLACK, 7, 1)
        )
                .collect(Collectors.toSet());
    }

    private Set<Piece> loadRooks() {
        return Stream.of(
                new Piece(ROOK, WHITE, 1, 8),
                new Piece(ROOK, BLACK, 1, 1),
                new Piece(ROOK, WHITE, 8, 8),
                new Piece(ROOK, BLACK, 8, 1)
        )
                .collect(Collectors.toSet());
    }

    private String generateName() {
        return attributes.get(random.nextInt(attributes.size())) + "_" + animals.get(random.nextInt(animals.size()));
    }
}
