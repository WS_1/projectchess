package com.projectchess.repository;

import com.projectchess.model.Piece;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PieceRepository extends JpaRepository<Piece, UUID> {
}
