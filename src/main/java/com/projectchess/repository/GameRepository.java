package com.projectchess.repository;

import com.projectchess.model.Game;
import com.projectchess.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Set;
import java.util.UUID;

@RepositoryRestResource(collectionResourceRel = "game", path = "game")
public interface GameRepository extends JpaRepository<Game, UUID> {
    Set<Game> findByInProgressIsTrue();

    Game findByIdAndWhitePlayerAndInProgressIsTrue(UUID id, User whitePlayer);
}
