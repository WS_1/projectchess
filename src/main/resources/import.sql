-- All user passwords are set to 12345678
INSERT INTO user (id, username, password)
VALUES ('b8314293-3ec8-4bba-b520-cbf662591283', 'admin', '$2a$10$ikcwCtABmyzt5gFNFZaYjetmrtueR5GaUFSxkmtQ90Oqn7ewQ/eWC');
INSERT INTO user (id, username, password)
VALUES ('c28f613c-91c1-4511-b70d-57487a85d2c5', 'user', '$2a$10$ikcwCtABmyzt5gFNFZaYjetmrtueR5GaUFSxkmtQ90Oqn7ewQ/eWC');
INSERT INTO user (id, username, password)
VALUES ('283fa182-434b-465f-bf00-73f23d0e813d', 'user_admin', '$2a$10$ikcwCtABmyzt5gFNFZaYjetmrtueR5GaUFSxkmtQ90Oqn7ewQ/eWC');

INSERT INTO role (id, authority)
VALUES ('6229e858-3220-447b-b563-eafbaa44164b', 'ROLE_ADMIN');
INSERT INTO role (id, authority)
VALUES ('4aa1b2e2-1365-462e-b5b6-c6d553045d7f', 'ROLE_USER');

INSERT INTO user_roles (user_id, roles_id)
VALUES ('b8314293-3ec8-4bba-b520-cbf662591283', '6229e858-3220-447b-b563-eafbaa44164b');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('c28f613c-91c1-4511-b70d-57487a85d2c5', '4aa1b2e2-1365-462e-b5b6-c6d553045d7f');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('283fa182-434b-465f-bf00-73f23d0e813d', '6229e858-3220-447b-b563-eafbaa44164b');
INSERT INTO user_roles (user_id, roles_id)
VALUES ('283fa182-434b-465f-bf00-73f23d0e813d', '4aa1b2e2-1365-462e-b5b6-c6d553045d7f');
