import React from 'react';
import Button from '@material-ui/core/Button';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {createGame} from '../../redux/actions/game';
import {func, object} from 'prop-types';

const NewGame = ({history, createGame}) => {
	const handleOnClick = () => {
		createGame(history);
	};

	return <Button variant="contained" color="primary" onClick={handleOnClick}>
		New Game
	</Button>;
};

NewGame.propTypes = {
	history: object,
	createGame: func,
};

const mapStateToProps = (state) => ({
	game: state.game,
});

const mapDispatchToProps = {
	createGame,
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(NewGame));
