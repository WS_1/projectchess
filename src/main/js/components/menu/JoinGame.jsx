import React, {Component, Fragment} from 'react';
import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import {withRouter} from 'react-router-dom';
import {withStyles} from '@material-ui/core/styles';
import {getGames} from '../../redux/actions/game';
import {connect} from 'react-redux';
import {array, func, object} from 'prop-types';
import {List} from '@material-ui/core';
import ListGames from './ListGames';

const styles = theme => ({
	modal: {
		position: 'absolute',
		top: '50%',
		left: '50%',
		transform: 'translate(-50%, -50%)',
		width: 400,
		backgroundColor: theme.palette.background.paper,
		border: '2px solid #000',
		boxShadow: theme.shadows[5],
		padding: theme.spacing(2, 4, 3),
	},
});

class JoinGame extends Component {
	constructor(props, context) {
		super(props, context);
		this.state = {
			open: false,
		};
	}

	static propTypes = {
		classes: object,
		inProgressGameIds: array,
		getGames: func,
	};

	handleButton = () => {
		this.props.getGames();
		this.handleOpen();
	};

	handleOpen = () => {
		this.setState({open: true});
	};

	handleClose = () => {
		this.setState({open: false});
	};

	render() {
		const {open} = this.state;
		const {classes, inProgressGameIds} = this.props;

		return <Fragment>
			<Button variant="contained" color="secondary" onClick={this.handleButton}>
				Join Game
			</Button>
			<Modal
				aria-labelledby="simple-modal-title"
				aria-describedby="simple-modal-description"
				open={open}
				onClose={this.handleClose}
			>
				<div className={classes.modal}>
					<h2 id="simple-modal-title"> Join Game </h2>
					<List component="nav" id="simple-modal-description" style={{maxHeight: 300, overflow: 'auto'}}>
						<ListGames links={inProgressGameIds}/>
					</List>
				</div>
			</Modal>
		</Fragment>;
	}
}

const mapStateToProps = ({game}) => {
	return {
		inProgressGameIds: game.inProgressGameIds,
	};
};

const mapDispatchToProps = {
	getGames,
};

export default connect(mapStateToProps, mapDispatchToProps)(
	withStyles(styles)(
		withRouter(JoinGame),
	),
);
