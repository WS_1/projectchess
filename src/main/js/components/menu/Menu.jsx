import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import NewGame from './NewGame';
import JoinGame from './JoinGame';

const useStyles = makeStyles(theme => ({
	paper: {
		maxWidth: 400,
		margin: `${theme.spacing(1)}px auto`,
		padding: theme.spacing(2),
	},
	modal: {
		position: 'absolute',
		top: '50%',
		left: '50%',
		transform: 'translate(-50%, -50%)',
		width: 400,
		backgroundColor: theme.palette.background.paper,
		border: '2px solid #000',
		boxShadow: theme.shadows[5],
		padding: theme.spacing(2, 4, 3),
	},
}));

const Menu = () => {
	const classes = useStyles();

	return <Paper className={classes.paper}>
		<Grid container spacing={2} style={{textAlign: 'center'}} align="center"
			justify="center"
			direction="column">
			<Grid item xs={12} sm={12}>
				<h1>Menu</h1>
			</Grid>
			<Grid item xs={12} sm={12}>
				<NewGame/>
			</Grid>
			<Grid item xs={12} sm={12}>
				<JoinGame/>
			</Grid>
		</Grid>
	</Paper>;
};

export default Menu;
