import {ListItem, ListItemIcon, ListItemText} from '@material-ui/core';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import React from 'react';
import {joinGame} from '../../redux/actions/game';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {array, func, object} from 'prop-types';

const ListGames = ({history, games, joinGame}) => {
	const handleOnClick = (id) => {
		return () => {
			joinGame(history, id);
		};
	};

	return games?.map(({id, name}) => {
		return (
			<ListItem button key={id} onClick={handleOnClick(id)}>
				<ListItemIcon>
					<PlayArrowIcon fontSize="small"/>
				</ListItemIcon>
				<ListItemText>
					{name}
				</ListItemText>
			</ListItem>);
	});
};

ListGames.propTypes = {
	history: object,
	openGame: func,
	games: array,
};

const mapStateToProps = (state) => ({
	games: state.game.inProgressGames,
});

const mapDispatchToProps = {
	joinGame,
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ListGames));
