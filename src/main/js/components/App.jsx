import React from 'react';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import lightBlue from '@material-ui/core/colors/lightBlue';
import CssBaseline from '@material-ui/core/CssBaseline';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import {Provider} from 'react-redux';
import configureStore from '../redux/store/configureStore';
import Loader from './Loader';
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import LogIn from './LogIn';
import DefaultContainer from './DefaultContainer';
import {Switch} from 'react-router';

const App = () => {
	const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');

	const theme = React.useMemo(
		() =>
			createMuiTheme({
				palette: {
					primary: {
						main: '#311b92',
					},
					secondary: lightBlue,
					type: prefersDarkMode ? 'dark' : 'light',
				},
			}),
		[prefersDarkMode],
	);

	return <Provider store={configureStore}>
		<ThemeProvider theme={theme}>
			<React.Fragment>
				<CssBaseline/>
				<Router>
					<Switch>
						<Route exact path="/" component={LogIn}/>
						<Route component={DefaultContainer}/>
					</Switch>
					<Loader/>
				</Router>
			</React.Fragment>
		</ThemeProvider>
		<ToastContainer
			position="bottom-left"
			autoClose={5000}
			hideProgressBar={false}
			newestOnTop={false}
			closeOnClick
			rtl={false}
			pauseOnFocusLoss
			draggable
			pauseOnHover
		/>
	</Provider>;
};

export default App;
