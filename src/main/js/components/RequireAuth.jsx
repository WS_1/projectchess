import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getUserDetails} from '../redux/actions/user';
import {withRouter} from 'react-router';
import {func, object, string} from 'prop-types';

export default function (ComposedComponent) {

	class Authentication extends Component {
		constructor(props) {
			super(props);
			props.getUserDetails(props.history);
		}

		// noinspection JSCheckFunctionSignatures
		componentDidUpdate(prevProps) {
			if (!prevProps.username) {
				prevProps.getUserDetails(prevProps.history);
			}
			return null;
		}

		render() {
			return <ComposedComponent {...this.props} />;
		}
	}

	Authentication.propTypes = {
		history: object,
		username: string,
		getUserDetails: func,
	};

	function mapStateToProps(state) {
		return {username: state.user.username};
	}

	const mapDispatchToProps = {
		getUserDetails,
	};

	return connect(mapStateToProps, mapDispatchToProps)(withRouter(Authentication));
}