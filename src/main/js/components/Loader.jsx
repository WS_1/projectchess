import React from 'react';
import {CircularProgress} from '@material-ui/core';
import {connect} from 'react-redux';
import Modal from '@material-ui/core/Modal';
import {withStyles} from '@material-ui/core/styles';
import {doneLoading, loading} from '../redux/actions/loader';
import apiConfiguration from '../redux/helper/apiConfiguration';
import {withRouter} from 'react-router';
import {bool, object} from 'prop-types';

const styles = {
	modal: {
		position: 'absolute',
		top: '50%',
		left: '50%',
		outline: 'none',
	},
};

class Loader extends React.Component {
	constructor(props, context) {
		super(props, context);
		apiConfiguration(this, props.history);
	}

	render() {
		let {isLoading, classes} = this.props;
		if (isLoading) {
			return <Modal open={true}>
				<CircularProgress className={classes.modal}/>
			</Modal>;
		} else {
			return <></>;
		}
	}
}

Loader.propTypes = {
	isLoading: bool,
	classes: object,
	history: object,
};

const mapStateToProps = ({loader: {isLoading}}) => ({isLoading});

const mapDispatchToProps = {
	loading,
	doneLoading,
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(withRouter(Loader)));
