import NavBar from './NavBar';
import {Route} from 'react-router-dom';
import Menu from './menu/Menu';
import Game from './game/Game';
import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import RequireAuth from './RequireAuth';

const useStyles = makeStyles(theme => ({
	root: {
		flexGrow: 1,
		padding: theme.spacing(3),
		minHeight: 'calc(100vh - 64px)',
	},
}));

const DefaultContainer = () => {
	const classes = useStyles();

	return (
		<>
			<NavBar/>

			<div className={classes.root}>
				<Route exact path="/menu" component={RequireAuth(Menu)}/>
				<Route path="/game" component={RequireAuth(Game)}/>
			</div>
		</>
	);
};

export default DefaultContainer;
