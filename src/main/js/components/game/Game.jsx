import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {func, object} from 'prop-types';
import {abandonGame, getGame} from '../../redux/actions/game';
import Button from '@material-ui/core/Button';
import Board from './Board';
import {Grid, makeStyles, Paper, Typography} from '@material-ui/core';
import Timer from './Timer';
import ConqueredPieces from './ConqueredPieces';
import MoveHistory from './MoveHistory';

const useStyles = makeStyles((theme) => ({
	paper: {
		paddingLeft: theme.spacing(2),
		paddingRight: theme.spacing(2),
		margin: 'auto',
		maxWidth: '90%',
	},
	info: {
		paddingLeft: '20px',
		paddingRight: '20px',
	},
	buttons: {
		display: 'flex',
		justifyContent: 'space-between',
		width: '100%',
		alignItems: 'center',
		paddingBottom: '20px',
		paddingTop: '20px',
	},
}));

function Game({history, location, game, getGame, abandonGame}) {
	const classes = useStyles();

	if (!game) {
		getGame(location.pathname.split(/[/ ]+/).pop());
	}

	const handleOnClick = () => {
		abandonGame(history, game.id);
	};

	return <Paper className={classes.paper}>
		<Grid container spacing={2}>
			<Grid item xs={12} sm container>
				<Grid item xs container direction="column" spacing={2}>
					<Grid item xs>
						<Typography gutterBottom variant="subtitle1">
                            Game:
							{game?.name}
						</Typography>
						<Board/>
					</Grid>
				</Grid>
				<Grid item className={classes.info}>
					<Grid item>
						<Typography variant="subtitle1">
							<ConqueredPieces/>
						</Typography>
					</Grid>
					<Grid item>
						<Typography variant="subtitle1">
							<MoveHistory/>
						</Typography>
					</Grid>
				</Grid>
			</Grid>
		</Grid>
		<Grid className={classes.buttons}>
			<Button variant="contained" color="primary"> Undo Move </Button>
			<Button variant="contained" color="secondary" onClick={handleOnClick}>
                Abandon Game
			</Button>
			<Timer/>
		</Grid>
	</Paper>;
}

Game.propTypes = {
	game: object,
	location: object,
	getGame: func,
	abandonGame: func,
	history: object,
};

const mapStateToProps = ({game}) => {
	return {
		game: game.inProgressGame,
	};
};

const mapDispatchToProps = {
	getGame,
	abandonGame,
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Game));

