import React, {useLayoutEffect, useState} from 'react';
import Tile from './Tile';
import {withStyles} from '@material-ui/core';
import {array, object} from 'prop-types';
import blueGrey from '@material-ui/core/colors/blueGrey';
import Pawn from './pieces/Pawn';
import {connect} from 'react-redux';
import King from './pieces/King';
import Rook from './pieces/Rook';
import Queen from './pieces/Queen';
import Bishop from './pieces/Bishop';
import Knight from './pieces/Knights';

const borderCol = blueGrey[500];

const styles = () => ({
	root: {
		display: 'flex',
		marginLeft: '45px',
		flexDirection: 'column',
	},
	label: {
		display: 'flex',
		flexDirection: 'row',
		color: borderCol,
		fontWeight: 'bold',
	},
	boardContainer: {
		display: 'flex',
		flexDirection: 'column',
		border: '3px',
		borderStyle: 'solid',
		borderColor: borderCol,
	},
	row: {
		display: 'flex',
	},
	column: {
		flexDirection: 'row',
	},
});

const idxArray = [...Array(8).keys()];
const rowIdxArray = [8, 7, 6, 5, 4, 3, 2, 1];
const columnIdxArray = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];

const minBoardSize = 200;
const coefficient = 0.65;

const pieceTypeToElement = {
	PAWN: (tileSize, color) => (<Pawn size={tileSize} color={color}/>),
	KING: (tileSize, color) => (<King size={tileSize} color={color}/>),
	QUEEN: (tileSize, color) => (<Queen size={tileSize} color={color}/>),
	BISHOP: (tileSize, color) => (<Bishop size={tileSize} color={color}/>),
	KNIGHT: (tileSize, color) => (<Knight size={tileSize} color={color}/>),
	ROOK: (tileSize, color) => (<Rook size={tileSize} color={color}/>),
};

const emptyPiece = () => (<div/>);

const Board = ({classes, pieces}) => {
	const [boardSize, setBoardSize] = useState(minBoardSize);

	useLayoutEffect(() => {
		const updateSize = () => {
			const minReference = Math.min(window.innerWidth, window.innerHeight);
			const valueToSet = Math.max(minReference * coefficient, minBoardSize);
			setBoardSize(valueToSet);
		};

		window.addEventListener('resize', updateSize);
		updateSize();
		return () => window.removeEventListener('resize', updateSize);
	}, []);

	const tileSize = (boardSize - 6) / 8;  // 6 from border*2

	const tileToPiece = pieces?.reduce((map, piece) => {
		const elementFactory = (pieceTypeToElement[piece.type] || emptyPiece);
		map[piece.posX] = { ...map[piece.posX], [piece.posY]:  elementFactory(tileSize, piece.color) };
		return map;
	}, {});

	return <div className={classes.root}>
		<div className={classes.label}>
			{columnIdxArray.map((columnIdx) => (
				<div key={columnIdx} style={{width: `${tileSize}px`, textAlign: 'center', paddingLeft: '10px'}}>
					{columnIdx}
				</div>))}
		</div>
		<div className={classes.label}>
			<div>
				{rowIdxArray.map((rowIdx) => (
					<div key={rowIdx} style={{height: `${tileSize}px`, lineHeight: `${tileSize}px`}}>
						{rowIdx}
					</div>))}
			</div>
			<div style={{height: `${boardSize}px`, width: `${boardSize}px`}} className={classes.boardContainer}>
				{
					idxArray.map((rowIdx) => (
						<div key={rowIdx} className={classes.row}>
							{
								idxArray.map((columnIdx) => {
									return (
										<div key={columnIdx} className={classes.column}>
											<Tile
												rowIdx={rowIdx}
												columnIdx={columnIdx}
												size={tileSize}
												piece={tileToPiece && tileToPiece[columnIdx+1] && tileToPiece[columnIdx+1][rowIdx+1]}
												key={rowIdx}
											/>
										</div>
									);
								})
							}
						</div>
					))
				}
			</div>
			<div>
				{rowIdxArray.map((rowIdx) => (
					<div key={rowIdx} style={{height: `${tileSize}px`, lineHeight: `${tileSize}px`}}>
						{rowIdx}
					</div>))}
			</div>
		</div>
		<div className={classes.label}>
			{columnIdxArray.map((columnIdx) => (
				<div key={columnIdx} style={{width: `${tileSize}px`, textAlign: 'center', paddingLeft: '10px'}}>
					{columnIdx}
				</div>))}
		</div>
	</div>;
};

Board.propTypes = {
	classes: object,
	pieces: array,
};

const mapStateToProps = ({game}) => {
	return {
		pieces: game.inProgressGame?.pieces,
	};
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Board));