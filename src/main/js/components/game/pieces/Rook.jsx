import React from 'react';
import RookWhite from '../../../../resources/static/chessSet/RookWhite.png';
import RookBlack from '../../../../resources/static/chessSet/RookBlack.png';
import {number, string} from 'prop-types';

const Rook = ({size ,color}) => {
	return (
		<div>
			<img src={color === 'WHITE' ? RookWhite: RookBlack} alt="White Rook" height={size} width={size}/>
		</div>
	);
};

Rook.propTypes = {
	size: number,
	color: string,
};

export default Rook;
