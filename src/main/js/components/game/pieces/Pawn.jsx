import React from 'react';
import PawnWhite from '../../../../resources/static/chessSet/PawnWhite.png';
import PawnBlack from '../../../../resources/static/chessSet/PawnBlack.png';
import {number, string} from 'prop-types';

const Pawn = ({size ,color}) => {
	return (
		<div>
			<img src={color === 'WHITE' ? PawnWhite: PawnBlack} alt="White pawn" height={size} width={size}/>
		</div>
	);
};

Pawn.propTypes = {
	size: number,
	color: string,
};

export default Pawn;
