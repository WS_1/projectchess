import React from 'react';
import BishopWhite from '../../../../resources/static/chessSet/BishopWhite.png';
import BishopBlack from '../../../../resources/static/chessSet/BishopBlack.png';
import {number, string} from 'prop-types';

const Bishop = ({size ,color}) => {
	return (
		<div>
			<img src={color === 'WHITE' ? BishopWhite: BishopBlack} alt="White Bishop" height={size} width={size}/>
		</div>
	);
};

Bishop.propTypes = {
	size: number,
	color: string,
};

export default Bishop;
