import React from 'react';
import KnightWhite from '../../../../resources/static/chessSet/KnightWhite.png';
import KnightBlack from '../../../../resources/static/chessSet/KnightBlack.png';
import {number, string} from 'prop-types';

const Knight = ({size ,color}) => {
	return (
		<div>
			<img src={color === 'WHITE' ? KnightWhite: KnightBlack} alt="White Knight" height={size} width={size}/>
		</div>
	);
};

Knight.propTypes = {
	size: number,
	color: string,
};

export default Knight;
