import React from 'react';
import KingWhite from '../../../../resources/static/chessSet/KingWhite.png';
import KingBlack from '../../../../resources/static/chessSet/KingBlack.png';
import {number, string} from 'prop-types';

const King = ({size ,color}) => {
	return (
		<div>
			<img src={color === 'WHITE' ? KingWhite: KingBlack} alt="White king" height={size} width={size}/>
		</div>
	);
};

King.propTypes = {
	size: number,
	color: string,
};

export default King;
