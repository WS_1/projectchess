import React from 'react';
import QueenWhite from '../../../../resources/static/chessSet/QueenWhite.png';
import QueenBlack from '../../../../resources/static/chessSet/QueenBlack.png';
import {number, string} from 'prop-types';

const Queen = ({size ,color}) => {
	return (
		<div>
			<img src={color === 'WHITE' ? QueenWhite: QueenBlack} alt="White Queen" height={size} width={size}/>
		</div>
	);
};

Queen.propTypes = {
	size: number,
	color: string,
};

export default Queen;
