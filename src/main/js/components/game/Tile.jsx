import React from 'react';
import {withStyles} from '@material-ui/core';
import {number, object} from 'prop-types';
import blueGrey from '@material-ui/core/colors/blueGrey';

const dark = blueGrey[900];
const light = blueGrey[50];

const styles = () => ({
	tileColorBlack: {
		backgroundColor: dark,
	},
	tileColorWhite: {
		backgroundColor: light,
	},
});

const Tile = ({classes, rowIdx, columnIdx, size, piece}) => {
	const isWhite = (rowIdx%2 === 0 && columnIdx%2 === 0) || (rowIdx%2 !== 0 && columnIdx%2 !== 0);

	return <div style={{height: `${size}px`, width: `${size}px`}} className={isWhite ? classes.tileColorWhite : classes.tileColorBlack}>
		{piece}
	</div>;
};

Tile.propTypes = {
	rowIdx: number,
	columnIdx: number,
	size: number,
	classes: object,
	piece: object,
};


export default withStyles(styles)(Tile);