import {makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import React, {useRef, useState} from 'react';
import Button from '@material-ui/core/Button';
import {func, object} from 'prop-types';
import {IconButton, InputAdornment, TextField} from '@material-ui/core';
import {Visibility, VisibilityOff} from '@material-ui/icons';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {login} from '../redux/actions/user';

const useStyles = makeStyles(theme => ({
	root: {
		flexGrow: 1,
		overflow: 'hidden',
		padding: theme.spacing(0, 3),
	},
	paper: {
		maxWidth: 400,
		margin: `${theme.spacing(1)}px auto`,
		padding: theme.spacing(2),
	},
}));

const LogIn = ({history, login}) => {
	const classes = useStyles();

	const usernameInput = useRef('');
	const passwordInput = useRef('');
	const [usernameError, setUsernameError] = useState(false);
	const [passwordError, setPasswordError] = useState(false);

	const handleOnClick = () => {
		let usernameValue = usernameInput.current.value;
		let passwordValue = passwordInput.current.value;

		setUsernameError(!usernameValue);
		setPasswordError(!passwordValue);

		// if (usernameValue && passwordValue){
		login(history, usernameValue, passwordValue);
		// }

	};

	const [showPassword, setShowPassword] = useState(false);
	const handleClickShowPassword = () => setShowPassword(!showPassword);

	return <div className={classes.root}>
		<Paper className={classes.paper}>
			<Grid container
				spacing={2}
				style={{textAlign: 'center', fontSize: 18, padding: 30}}
				align="center"
				justify="center"
				direction="column">
				<div> Please Log In:</div>
				<div>
					<TextField
						error={usernameError}
						helperText={usernameError && 'Username required.'}
						id="user-input"
						label="Username"
						autoComplete="current-user"
						fullWidth
						inputRef={usernameInput}
					/>
				</div>
				<div>
					<TextField
						error = {passwordError}
						helperText={passwordError && 'Password required.'}
						id="password-input"
						label="Password"
						type={showPassword ? 'text' : 'password'}
						autoComplete="current-password"
						InputProps={{
							endAdornment: (
								<InputAdornment position="end">
									<IconButton aria-label="toggle password visibility"
										onClick={handleClickShowPassword}>
										{showPassword ? <Visibility/> : <VisibilityOff/>}
									</IconButton>
								</InputAdornment>
							),
						}}
						fullWidth
						inputRef={passwordInput}
					/>
				</div>
				<br/>
				<div>
					<Button variant="contained" color="primary" fullWidth onClick={handleOnClick}>Login</Button>
				</div>
			</Grid>
		</Paper>
	</div>;
};

LogIn.propTypes = {
	history: object,
	login: func,
};

const mapStateToProps = (state) => ({
	user: state.user,
});

const mapDispatchToProps = {
	login,
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(LogIn));
