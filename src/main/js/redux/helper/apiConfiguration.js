import axios from 'axios';
import {toast} from 'react-toastify';

export default (loaderComponent, history) => {
	axios.interceptors.request.use(function (config) {
		config.withCredentials = true;
    
		loaderComponent.props.loading();

		return config;
	});

	axios.interceptors.response.use(function (response) {
		loaderComponent.props.doneLoading();
		return response;
	}, function (error) {
		const statusCode = error.response ? error.response.status : null;

		switch (statusCode) {
		case 404:
			toast.error('Content was not found!');
			break;
		case 500:
			toast.error('There was an internal server error!');
			break;
		case 401:
			toast.error('User non-existent or not authorized!');
			history.push('/');
			break;
		default:
			toast.error('An error occurred!');
		}

		loaderComponent.props.doneLoading();
		return Promise.reject(error);
	});
};
