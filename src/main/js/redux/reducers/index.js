import {combineReducers} from 'redux';
import game from './game';
import loader from './loader';
import user from './user';

export default combineReducers({
	loader,
	game,
	user,
});
