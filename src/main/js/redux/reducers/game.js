import {ADD_IN_PROGRESS_GAME, ADD_IN_PROGRESS_GAMES} from '../actions/game';

export default (state = {inProgressGames: [], inProgressGame: null}, action) => {
	switch (action.type) {
	case ADD_IN_PROGRESS_GAME:
		return {
			...state,
			inProgressGame: action.game,
		};
	case ADD_IN_PROGRESS_GAMES:
		return {
			...state,
			inProgressGames: action.inProgressGames.map(game => ({id: game.id, name: game.name})),
		};
	default:
		return state;
	}
};
