import {LOGIN, LOGOUT, SET_USER} from '../actions/user';

export default (state = {}, action) => {
	switch (action.type) {
	case LOGIN:
		return {...action.user};
	case LOGOUT:
		return {};
	case SET_USER:
		return {...action.user};
	default:
		return state;
	}
};
