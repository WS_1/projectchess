import {DONE_LOADING, LOADING} from '../actions/loader';

export default (state = {isLoading: false}, action) => {
	switch (action.type) {
	case LOADING:
		return {...state, isLoading: true};
	case DONE_LOADING:
		return {...state, isLoading: false};
	default:
		return state;
	}
};
