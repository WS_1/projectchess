import axios from 'axios';

export const ADD_IN_PROGRESS_GAME = 'ADD_IN_PROGRESS_GAME';
export const ADD_IN_PROGRESS_GAMES = 'ADD_IN_PROGRESS_GAMES';

const GAME_URL = '/api/game';

export const addInProgressGame = (game) => ({
	type: ADD_IN_PROGRESS_GAME,
	game,
});

export const addInProgressGames = (inProgressGames) => ({
	type: ADD_IN_PROGRESS_GAMES,
	inProgressGames,
});

export const createGame = (history) => async (dispatch) => {
	const game = (await axios.post(GAME_URL)).data;
	dispatch(addInProgressGame(game));
	history.push(`/game/${game.id}`);
};

export const getGames = () => async (dispatch) => {
	const response = await axios.get(`${GAME_URL}/active`);
	dispatch(addInProgressGames(response.data));
};

export const getGame = (gameId) => async (dispatch) => {
	const game = (await axios.get(`${GAME_URL}/${gameId}`)).data;
	dispatch(addInProgressGame(game));
};

export const joinGame = (history, gameId) => (dispatch) => {
	dispatch(getGame(gameId));
	history.push(`/game/${gameId}`);
};

export const abandonGame = (history, gameId) => async () => {
	await axios.get(`${GAME_URL}/abandon/${gameId}`);
	history.push('/menu');
};
