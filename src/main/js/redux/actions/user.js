import axios from 'axios';

export const LOGIN = 'LOG_USER_IN';
export const LOGOUT = 'LOG_USER_OUT';
export const SET_USER = 'SET_USER';

const LOGIN_URL = '/api/login';
const LOGOUT_URL = '/api/logout';
const USER_DETAILS_URL = '/api/user-details';

export const loginSuccess = (user) => ({
	type: LOGIN,
	user: user,
});

export const logoutSuccess = () => ({
	type: LOGOUT,
});

export const userDetails = (user) => ({
	type: SET_USER,
	user: user,
});

export const login = (history, username, password) => async (dispatch) => {
	const loginDetails = (await axios.post(
		LOGIN_URL, {
			username: 'admin',
			password: '12345678',
		},
	)
	).data;

	dispatch(loginSuccess({...loginDetails}));
	history.push('/menu');
};

export const logout = (history) => async (dispatch) => {
	await axios.post(LOGOUT_URL);

	dispatch(logoutSuccess());
	history.push('/');
};

export const getUserDetails = (history) => async (dispatch) => {
	const user = await axios.get(USER_DETAILS_URL);
	dispatch(userDetails({...user.data}));

	if (!user.data.username) {
		history.push('/');
	}
};