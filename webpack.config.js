const {resolve} = require("path");
const HtmlWebPackPlugin = require("html-webpack-plugin");

module.exports = (env, argv) => ({
    devServer: {
        port: 3000,
        proxy: {
            '/api': {
                target: 'http://localhost:8080'
            }
        },
        historyApiFallback: true
    },
    entry: resolve(__dirname, "src/main/js/app.jsx"),
    output: {
        path: resolve(__dirname, "src/main/resources/static/"),
        publicPath: "/",
        filename: ["development", "production"].includes(argv.mode) ? "[name].[hash].bundle.js" : "[name].bundle.js"
    },
    resolve: {
        extensions: [".js", ".jsx"]
    },
    module: {
        rules: [
            {
                test: /\.jsx$/,
                include: resolve(__dirname, "src/main/js"),
                use: {
                    loader: "babel-loader",
                    options: {
                        cacheDirectory: true,
                        presets: ["@babel/preset-env", "@babel/preset-react"],
                        "plugins": [
                            ["@babel/plugin-proposal-decorators", { "legacy": true}],
                            ["@babel/plugin-proposal-class-properties"],
                            ["@babel/plugin-proposal-object-rest-spread"],
                            ["@babel/plugin-transform-destructuring"]
                        ]
                    }
                }
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader",
                        options: {minimize: true}
                    }
                ]
            },
            {
                test: /(\.css$)/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.(png|woff|woff2|eot|ttf|svg)$/,
                loader: "url-loader?limit=100000"
            }
        ]
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: resolve(__dirname, "src/main/js/index.html")
        })
    ]
});
