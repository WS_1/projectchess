# ProjectChess

A web based Chess game for 2 players.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

This is what you need to have installed in order to run the application locally:
* Git
* Java Jdk 1.8
* NPM

### Installing
All the following commands are to be executed in the project root directory.

1. Run `gradlew clean build`.
2. Run `npm run preBuild`.

### Running

1. Run `gradlew bootRun` to run the backend application.
2. Run `npm run devServer`.
3. Application will be available at:
   * backend: http://localhost:8080/
   * frontend: http://localhost:3000/

## Running the tests

TODO

## Built With

* [JAVA1.8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) - Backend language
* [ES9](https://www.ecma-international.org/ecma-262/9.0/index.html) - Frontend language
* [Gradle](https://gradle.org/) - Backend library dependency management 
* [NPM](https://www.npmjs.com/) - Frontend library dependency management
* [SpringBoot](https://spring.io/projects/spring-boot) - Web server application framework
* [H2](https://www.h2database.com/html/main.html) - File-based SQL database
* [React](https://reactjs.org/) - Javascript frontend framework
* [WebPack](https://webpack.js.org/) - Frontend module bundler
* [Babel](https://babeljs.io/) - Javascript ES9 compiler
* [Redux](https://redux.js.org/) - Javascript application container

## Contributing

* **Ioana Boian**
* **Lucian Gabriel Ilie**
